from django.contrib import admin
from django.contrib.admin.decorators import display
from .models import Products, Order

admin.site.site_header = "E-commerce site"
admin.site.site_title = "Shopping"
admin.site.index_title="Manage shopping"

class ProductAdmin(admin.ModelAdmin):
    def change_category_to_default(self,request,queryset):
        queryset.update(category="default")
    change_category_to_default.short_description='default category'
    
    list_display=('title','price','category', 'description')
    search_fields=('title', )
    actions=('change_category_to_default',)
    list_editable=('price',)

admin.site.register(Products, ProductAdmin)
admin.site.register(Order)
